releases:
  - name: minio
    namespace: rook-ceph
    chart: ./chart
    values:
      - services:
          minio:
            access_key: "{{ .Values.services.minio.access_key }}"
            secret_key: "{{ .Values.services.minio.secret_key }}"
            backup:
              schedule: "{{ .Values.services.minio.backup.schedule }}"
              enabled: {{ .Values.services.minio.backup.enabled }}
              host: "{{ .Values.services.minio.backup.host }}"
              access_key: "{{ .Values.services.minio.backup.access_key }}"
              secret_key: "{{ .Values.services.minio.backup.secret_key }}"
              directory: "{{ .Values.services.minio.backup.directory }}"
        cluster:
          domain: "{{ .Values.cluster.domain }}"
          tls_wildcard_secret: "{{ .Values.cluster.tls_wildcard_secret }}"
        rgw_public_domain: "rgw.{{ .Values.cluster.domain }}"
