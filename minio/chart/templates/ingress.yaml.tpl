apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    ingress.kubernetes.io/proxy-body-size: "20m"
    nginx.ingress.kubernetes.io/proxy-body-size: "20m"
    ingress-waf/additional-crs: |
      SecAction "id:900200,phase:1,nolog,pass,t:none,setvar:\'tx.allowed_methods=GET HEAD POST PUT DELETE PATCH OPTIONS\'"
      SecRuleRemoveById 920340 920420
  name: minio
spec:
  rules:
    - host: minio.{{ .Values.cluster.domain }}
      http:
        paths:
          - backend:
              service:
                name: minio
                port:
                  number: 9000
            path: /
            pathType: ImplementationSpecific
    - host: minio-ui.{{ .Values.cluster.domain }}
      http:
        paths:
          - backend:
              service:
                name: minio
                port:
                  number: 9001
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - minio.{{ .Values.cluster.domain }}
      secretName: "{{ .Values.cluster.tls_wildcard_secret }}"
