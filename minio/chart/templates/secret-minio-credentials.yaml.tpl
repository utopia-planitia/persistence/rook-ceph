apiVersion: v1
kind: Secret
metadata:
  name: minio-credentials
type: generic
stringData:
  MINIO_ACCESS_KEY: "{{ .Values.services.minio.access_key }}"
  MINIO_SECRET_KEY: "{{ .Values.services.minio.secret_key }}"
