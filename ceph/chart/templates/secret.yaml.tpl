apiVersion: v1
kind: Secret
metadata:
  name: rook-ceph-dashboard-password
  namespace: rook-ceph
type: kubernetes.io/rook
stringData:
  password: "{{ .Values.services.ceph.dashboard.password }}"
