#!/usr/bin/env bats

cleanup() {
  kubectl delete -f rgw-bucket-public-claim.yaml -f rgw-access-public.yaml --ignore-not-found=true
}

setup() {
  cleanup
}

teardown() {
  kubectl -n rook-ceph logs job/rgw-access-public

  cleanup

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "test public rgw" {
  run kubectl apply -f rgw-bucket-public-claim.yaml -f rgw-access-public.yaml
  [ $status -eq 0 ]
  sleep 1

  run kubectl -n rook-ceph wait --for=condition=complete --timeout=60s job/rgw-access-public
  [ $status -eq 0 ]
}
